//
// Created by 章先生 on 2020-02-06.
//

#include "common.h"

#include <assert.h>

/**===================================
 * 异常相关
 ====================================*/
void throwRuntimeException(JNIEnv *jEnv, const char *msg) {
    assert(jEnv != NULL);

    JNIEnv env = *jEnv;

    jclass clazz = env->FindClass(jEnv, "java/lang/RuntimeException");

    env->ThrowNew(jEnv, clazz, msg);
}