//
// Created by 章先生 on 2020-02-06.
//

#ifndef JNI_THREADPOOL_C_COMMON_H
#define JNI_THREADPOOL_C_COMMON_H

#include <jni.h>

/**===================================
 * 异常相关
 ====================================*/
/**
 * 抛出运行时异常
 *
 * @param jEnv
 * @param msg
 */
void throwRuntimeException(JNIEnv *jEnv, const char *msg);

#endif //JNI_THREADPOOL_C_COMMON_H
