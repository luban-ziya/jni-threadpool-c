//
// Created by 章先生 on 2020-02-06.
//

#include "com_qimingnan_jni_threadpool_MyThread.h"
#include "common.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <assert.h>

JNIEXPORT void JNICALL Java_com_qimingnan_jni_threadpool_MyThread_run0(JNIEnv *jEnv, jobject jobj) {
    JNIEnv env = *jEnv;

    /* 获取MyThread字节码 */
    jclass threadClass = env->GetObjectClass(jEnv, jobj);
    if (NULL == threadClass) {
        throwRuntimeException(jEnv, "获取线程类字节码失败");

        return;
    }

    /* 获取线程运行函数 */
    jmethodID runMethod = env->GetMethodID(jEnv, threadClass, "run", "()V");
    if (NULL == runMethod) {
        throwRuntimeException(jEnv, "获取线程运行函数失败");

        return;
    }

    /* 执行线程运行函数 */
    env->CallVoidMethod(jEnv, jobj, runMethod);
}